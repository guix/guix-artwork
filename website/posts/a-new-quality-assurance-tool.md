title: A new Quality Assurance tool for Guix
author: Christopher Baines
tags: Continuous integration, Quality Assurance
date: 2023-09-12 14:30:00
---

Maintaining and expanding Guix's collection of packages can be
complicated.  As a distribution with around 22,000 packages, spanning
across around 7 architectures and with support for cross-compilation,
it's quite common for problems to occur when making changes.

Quality Assurance (QA) is a general term to describe the approach
taken to try and ensure something meets expectations.  When applied to
software, the term testing is normally used.  While Guix is software,
and has tests, much more than those tests are needed to maintain Guix
as a distribution.

So what might quality relate to in the context of Guix as a
distribution?  This will differ from person to person, but these are
some common concerns:

 - Packages successfully building (both now, and without any
   [*time bombs*](https://issues.guix.gnu.org/56137) for the future)
 - The packaged software functioning correctly
 - Packages building on or for a specific architecture
 - Packages building reproducibly
 - Availability of translations for the package definitions

# Tooling to help with Quality Assurance

There's a range of tools to help maintain Guix.  The [package
linters](https://guix.gnu.org/en/manual/en/html_node/Invoking-guix-lint.html)
are a set of simple tools, they cover basic things from the naming of
packages to more complicated checkers that look for security issues
for example.

The [`guix weather`](https://guix.gnu.org/en/manual/en/html_node/Invoking-guix-weather.html)
tool looks at substitute availability information and can indicate how
many substitutes are available for the current Guix and system.  The
[`guix challenge`](https://guix.gnu.org/en/manual/en/html_node/Invoking-guix-challenge.html)
tool is similar, but it highlights package reproducibility issues,
which is when the substitutes and local store items (if available)
differ.

For translations, [Guix uses
Weblate](https://guix.gnu.org/manual/en/html_node/Translating-Guix.html)
which can provide information on how many translations are available.

# The QA front-page

Then there's the relatively new [Quality Assurance (QA)
front-page](https://qa.guix.gnu.org/), the aim of which is to bring
together some of the existing Quality Assurance related information,
as well as new being a good place to do additional QA tasks.

The QA front-page
[started](https://lists.gnu.org/archive/html/guix-devel/2022-09/msg00054.html)
as a service to coordinate automated testing for patches. When a patch
or patch series is submitted to guix-patches@gnu.org, it is
automatically applied to create a branch; then once the information is
available from the [Data Service](https://data.qa.guix.gnu.org/) about
this branch, the QA front-page web interface lets you view which
packages were modified and submits builds for these changes to the
[Build Coordinator](https://guix.gnu.org/en/blog/2021/building-derivations-how-complicated-can-it-be/)
behind [bordeaux.guix.gnu.org](https://guix.gnu.org/en/blog/2021/substitutes-now-also-available-from-bordeauxguixgnuorg/)
to provide build information about the modified packages.

![QA issue page](/static/blog/img/qa-issue.png)

A very similar process applies for branches other than the master
branch, the QA front-page queries
[issues.guix.gnu.org](https://issues.guix.gnu.org/) to find out which
branch is going to be merged next, then follows the same process for
patches.

For both patches and branches the QA front-page displays information
about the effects of the changes.  When this information is available,
it can assist with reviewing the changes and help get patches merged
quicker. This is a work in progress though, and there's much more that
the QA front-page should be able to do as providing clearer
descriptions of the changes or any other problems that should be
addressed.
![QA package changes page](/static/blog/img/qa-package-changes.png)

# How to get involved?

There's plenty of ways to get involved or contribute to the QA
front-page.

If you submit patches to Guix, the QA front-page will attempt to apply
the patches and show what's changed. You can click through from
issues.guix.gnu.org to [qa.guix.gnu.org](https://qa.guix.gnu.org/) via
the QA badge by the status of the issue.

From the QA front-page, you can also view the list of branches which
includes the requests for merging if they exist.  Similar to the patch
series, for the branch the QA front-page can display information about
the package changes and substitute availability.

There's also plenty of ways to contribute to the QA front-page and
connected tools.  You can find some ideas and information on how to
run the service in the [README](https://qa.guix.gnu.org/README) and if
you have any questions or patches, please email `guix-devel@gnu.org`.

# Acknowledgments

Thanks to Simon Tournier and Ludovic Courtès for providing feedback on
an earlier draft of this post.

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package manager
and an advanced distribution of the GNU system that [respects user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the Hurd or the Linux
kernel, or it can be used as a standalone operating system
distribution for i686, x86_64, ARMv7, AArch64 and POWER9 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package
management, per-user profiles, and garbage collection.  When used as a
standalone GNU/Linux distribution, Guix offers a declarative,
stateless approach to operating system configuration management.  Guix
is highly customizable and hackable through
[Guile](https://www.gnu.org/software/guile) programming interfaces and
extensions to the [Scheme](http://schemers.org) language.
