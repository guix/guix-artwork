title: Take the Guix User and Contributor Survey
slug: guix-user-contributor-survey-2024
date: 2024-11-10 10:01:00
author: Steve George
tags: Community
---

To understand the views of the Guix community we're running a survey that we'd
love **you** to take part in! The
[Guix User and Contributor Survey](https://guix.limesurvey.net/) is live
now, and should take about 10 minutes to fill out. Perfect for doing with a cup
of tea and a biscuit!

The Guix project continues to grow and change, with new contributors and users
joining our community. We decided to run this survey as it's the best way to
gather good quality feedback across the widest cross-section of the community.
Of course, there's lots of interesting topics a survey could ask about! We
decided to focus on how Guix is used, and how contributors take part in the
project.

The survey is being run on
[LimeSurvey](https://en.wikipedia.org/wiki/LimeSurvey) which is a Free Software
project and has been used by many other projects for similar surveys. The
survey's hosted on the [LimeSurvey SaaS](https://www.limesurvey.org/) so that we
don't have the additional task of operating the software. No personal data is
asked for (e.g. email addresses), no tracking data is being
collected (e.g. IP addresses) and the entries are anonymised. 

We'll be making the results and the anonymised data available under the
[Creative Commons CCO](https://creativecommons.org/public-domain/cc0/):
that way anyone can analyse the data for further insights.

We hope the results of the survey will be used to understand both the Guix
project's strengths and areas we can improve. Which is why your input is
so important.  If you can, please take the survey!

[Take the survey now!](https://guix.limesurvey.net/)


