title: Meet Guix at FOSDEM
slug: meet-guix-at-fosdem-2023
date: 2023-01-23 15:00:00
author: Ludovic Courtès
tags: FOSDEM, Guix Days, Talks, Community
---

GNU Guix will be present at [FOSDEM](https://fosdem.org/2023/) next
week, February 4th and 5th.  This is the first time since the pandemic
that FOSDEM takes place again “in the flesh” in Brussels, which is
exciting to those of us lucky enough to get there!  Everything will be
live-streamed and recorded thanks to the amazing FOSDEM crew, so
everyone can enjoy wherever they are; some of the talks this year will
be “remote” too: pre-recorded videos followed by live Q&A sessions with
the speaker.

Believe it or not, it’s the [9th year Guix is represented at
FOSDEM](https://guix.gnu.org/en/blog/tags/fosdem/), with more than 30
talks given in past editions!  This year brings several talks that will let you
learn more about different areas of the joyful Hydra Guix has become.

This all starts on Saturday, in particular with the amazing [declarative
and minimalistic computing
track](https://fosdem.org/2023/schedule/track/declarative_and_minimalistic_computing/):

  - [“_Bringing RISC-V to Guix's
    bootstrap_”](https://fosdem.org/2023/schedule/event/guixriscv/)
    (remote), as a continuation of [last year’s
    talk](https://fosdem.org/2022/schedule/event/riscvadventures/), will
    be Ekaitz Zarraga’s account of the successful port the _full-source
    bootstrap_ to RISC-V—no less!
  - In [“_Using GNU Guix Containers with FHS (Filesystem Hierarchy
    Standard)
    Support_”](https://fosdem.org/2023/schedule/event/guixfhs/) (remote)
    John Kehayias will present the recently-added [`guix shell
    --container
    --emulate-fhs`](https://guix.gnu.org/en/blog/2023/the-filesystem-hierarchy-standard-comes-to-guix-containers/).
  - [“_Declaring just what is
    necessary_”](https://fosdem.org/2023/schedule/event/minimalguixsystemimages/)
    (remote) will show how to create system images that contain just
    what you need, by Efraim Flashner.
  - In [“_GNU Guix and Open Science, a
    crush?_”](https://fosdem.org/2023/schedule/event/guixopenscience/),
    Simon Tournier will illustrates ways in which Guix can be beneficial
    to “open science”.
  - [“_How Replicant, a 100% free software Android distribution, uses
    (or doesn't use)
    Guix_”](https://fosdem.org/2023/schedule/event/replicantguix/) will
    showcase an unusual and exciting use case for Guix, by one of
    Replicant’s core developers, Denis “GNUtoo” Carikli.
  - [“_An Introduction to Guix
    Home_”](https://fosdem.org/2023/schedule/event/guixhome/) will be
    given on Sunday (remote) by David Wilson of [System
    Crafters](https://systemcrafters.net/) fame—a must if you want to
    understand this newfangled Guix Home thing!

There are many other [exciting talks in this
track](https://fosdem.org/2023/schedule/track/declarative_and_minimalistic_computing/),
some of which closely related to Guix and Guile; check it out!

You can also discover Guix in other tracks:

  - On Saturday, [“_Guix, toward practical transparent, verifiable and
    long-term reproducible
    research_”](https://fosdem.org/2023/schedule/event/openresearch_guix/)
    will be an introduction to Guix (by Simon Tournier) for an audience
    of scientists interested in coming up with scientific practices that
    improves verifiability and transparency.
  - On Saturday in the security track, [_“Where does that code come
    from?”_](https://fosdem.org/2023/schedule/event/security_where_does_that_code_come_from/)
    (by Ludovic Courtès) will talk Git checkout authentication in Guix
    and how this fits in the broader picture of “software supply chain”
    security.
  - On Sunday, Efraim Flashner will talk about [“_Porting RISC-V to
    GNU Guix_”](https://fosdem.org/2023/schedule/event/rv_gnu_guix/) in
    the RISC-V track.
  - On Sunday, in the high-performance computing (HPC) track, Ludovic
    Courtès will give a lightning talk about CPU tuning in Guix entitled
    [“_Reproducibility and performance: why
    choose?_”](https://fosdem.org/2023/schedule/event/cpu_tuning_gnu_guix/).

![Guix Days logo](/static/blog/img/Guix-Days-2023.png)

As was the case pre-pandemic, we are also organizing the Guix Days as a
[FOSDEM fringe event](https://fosdem.org/2023/fringe/), a two-day Guix
workshop where contributors and enthusiasts will meet.  The workshop
takes place on **Thursday Feb. 2nd and Friday Feb. 3rd** at the
[Institute of Cultural Affairs (ICAB) in
Brussels](https://www.openstreetmap.org/?mlat=50.85025&mlon=4.37326#map=19/50.85025/4.37326).

Again this year there will be few talks; instead, the event will
consist primarily of
“[unconference-style](https://en.wikipedia.org/wiki/Unconference)”
sessions focused on specific hot topics about Guix, the Shepherd,
continuous integration, and related tools and workflows.

Attendance to the workshop is free and open to everyone, though you are
invited to register (there are few seats left!).  Check out [the
workshop’s wiki
page](https://libreplanet.org/wiki/Group:Guix/FOSDEM2023) for
registration and practical info.  Hope to see you in Brussels!

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package manager and
an advanced distribution of the GNU system that [respects user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the Hurd or the Linux
kernel, or it can be used as a standalone operating system distribution
for i686, x86_64, ARMv7, AArch64, and POWER9 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.
