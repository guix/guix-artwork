title: Wrapping up Ten Years of Guix in Paris
author: Ludovic Courtès, Tanguy Le Carrour, Simon Tournier
tags: Community, Conference, Talks
date: 2022-09-28 16:30:00
---

Two weeks ago, some of us were in Paris, France, [to celebrate ten years
of Guix](https://10years.guix.gnu.org)!  The event included 22 talks and
12 lightning talks, covering topics ranging from reproducible research
on [Friday](https://10years.guix.gnu.org/program/#Friday) and Guix
hacking on [Saturday](https://10years.guix.gnu.org/program/#Saturday)
and [Sunday](https://10years.guix.gnu.org/program/#Sunday).

If you couldn’t make it in Paris, and if you missed the live stream, we
have some good news: videos of the talks and supporting material [are
now available from the program
page](https://10years.guix.gnu.org/program/)!

If you weren’t there, there are things you definitely missed though:
more than 60 participants from a diverse range of backgrounds—a rare
opportunity for scientists and hackers to meet!—, impromptu discussions
and encounters, and of course not one but *two* crazy birthday cakes
(yup! on one day it was vanilla/blueberry-flavored, and on the other day
it was chocolate/passion fruit, but both were equally beautiful!).

![Picture of the Guix birthday cake.](https://10years.guix.gnu.org/static/images/photos/2022_0917_15530400.small.jpg)

There are [a few more pictures](https://10years.guix.gnu.org/photos) on
the web site.

It might seem a bit of a stretch at first, but there *is* a connection
between, say, [bioinformatics
pipelines](https://10years.guix.gnu.org/video/reproducibility-of-bioinformatics-pipelines/),
[OCaml
bootstrapping](https://10years.guix.gnu.org/video/camlboot-debootstrapping-the-ocaml-compiler/),
and [Guix
Home](https://10years.guix.gnu.org/video/an-introduction-to-guix-home/):
it’s about deploying complex software stacks in a way that is not only
convenient but also transparent and reproducible.  It’s about retaining
control, both collectively and individually, over the “software supply
chain” at a time when the most popular option is to give up.

We have lots of people to thank, starting with the speakers and
participants: thanks for sharing your knowledge and enthusiasm, and
thank you for making it a warm and friendly event!  Thanks to the
[sponsors of the event](https://10years.guix.gnu.org/sponsors) without
which all this would have been impossible.

Special thanks to Nicolas Dandrimont of the Debian video team for
setting up the video equipment, tirelessly working during all three days
and even afterwards to prepare the “final cut”—you rock!!  Thanks to Leo
Famulari for setting up the live streaming server on short notice, and
to Luis Felipe for designing the unanimously acclaimed Ten Years of Guix
graphics, the kakemono, and the video intros and outros (check out [the
freely-licensed SVG
source](https://git.savannah.gnu.org/cgit/guix/guix-artwork.git/tree/promotional/)!),
all that under pretty tight time constraints.  Thanks also to Andreas
Enge with their Guix Europe hat on for addressing last-minute hiccups
behind the scenes.

Organizing this event has certainly been exhausting, but seeing it come
true and meeting both new faces and old-timers was a great reward for
us.  Despite the occasional shenanigans—delayed talks, one talk
cancellation, and worst of all: running out of coffee and tea after
lunch—we hope it was enjoyable for all.

For those in Europe, our next in-person meeting is probably going to be
FOSDEM.  And maybe this will inspire some to organize events in other
regions of the world and/or on-line meetups!

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package manager and
an advanced distribution of the GNU system that [respects user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the Hurd or the Linux
kernel, or it can be used as a standalone operating system distribution
for i686, x86_64, ARMv7, AArch64, and POWER9 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.
