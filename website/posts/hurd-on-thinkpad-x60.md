title: Guix/Hurd on a Thinkpad X60
subtitle: four years of progress
date: 2024-11-24 18:00
author: Janneke Nieuwenhuizen
slug: hurd-on-thinkpad
tags: GNU/Hurd
---

A lot has happened with respect to [the Hurd](https://hurd.gnu.org)
since our [_Childhurds and GNU/Hurd
Substitutes_](https://guix.gnu.org/en/blog/2020/childhurds-and-substitutes/)
post.  As long as two years ago some of you [have been
asking](https://logs.guix.gnu.org/hurd/2022-05-15.log#070056) for a
progress update and although there [have been
rumours](https://dezyne.org/janneke/logs/%23guix/2023-05-28.log.html#113945)
on a new blog post for over a year, [we
were](https://logs.guix.gnu.org/guix/2023-03-11.log#221532) kind of
[waiting for](https://logs.guix.gnu.org/guix/2023-05-13.log#165020)
the [rumoured x86_64
support](https://logs.guix.gnu.org/guix/2023-09-16.log#163627).

With all the exciting progress on the Hurd coming available after the
recent (last?) [merger of
`core-updates`](https://lists.gnu.org/archive/html/guix-devel/2024-08/msg00195.html)
we thought it better [not to
wait](https://logs.guix.gnu.org/guix/2023-09-16.log#163627) any
longer.  So here is a short overview of our Hurd work over the past
years:

  * Update [Hurd to 3ff7053, gnumach 1.8+git20220827, and fix build
    failures](https://lists.gnu.org/archive/html/guix-patches/2023-03/msg02186.html),

  * A [native compilation fix for
    gcc-boot0](https://lists.gnu.org/archive/html/bug-guix/2023-05/msg00526.html),

  * Initial [rumpdisk
    support](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg00633.html),
    more on this below, which [needed to
    wait](https://lists.gnu.org/archive/html/guix-patches/2023-07/msg00427.html)
    for:

  * A libc [specific to
    Hurd](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg00973.html),
    updating gnumach to 1.8+git20221224 and hurd to 0.9.git20230216,

  * Some 40 native [package build fixes for the
    Hurd](https://lists.gnu.org/archive/html/guix-patches/2023-07/msg00643.html)
    so that all development dependencies of the guix package are now
    available,

  * A hack to use Git source in commencement to [update and fix cross
    build and native build for the
    Hurd](https://lists.gnu.org/archive/html/guix-patches/2023-07/msg00630.html),

  * Support for buiding `guix` natively on the Hurd by splitting the
    [build into more steps for 32-bit
    hosts](https://lists.gnu.org/archive/html/bug-guix/2023-08/msg00190.html)

  * Even nicer offloading support for Childhurds by [introducing Smart
    Hurdloading](https://lists.gnu.org/archive/html/guix-patches/2023-09/msg01505.html)
    so that now both the Bordeaux and Berlin build farms build
    packages for i586-gnu,

  * Locale fixes for [wrong `glibc-utf8-locales` package used on
    GNU/Hurd](https://lists.gnu.org/archive/html/bug-guix/2023-10/msg00223.html),

  * More locale fixes to [use `glibc-utf8-locales/hurd` in
    %standard-patch-inputs](https://lists.gnu.org/archive/html/guix-patches/2023-11/msg01749.html),

  * And even more locale fixes for [using the right locales on
    GNU/Hurd](https://lists.gnu.org/archive/html/guix-patches/2023-11/msg01932.html),

  * A [new glibc
    2.38](https://lists.gnu.org/archive/html/guix-patches/2023-12/msg00343.html)
    allowing us to do `(define-public glibc/hurd glibc)`—i.e., once
    again use the same glibc for Linux and Hurd alike, and: Better
    Hurd support!,

  * Creation of [`hurd-team`
    branch](https://lists.gnu.org/archive/html/guix-commits/2024-01/msg01382.html)
    with build fixes, updating gnumach to 1.8+git20230410 and hurd to
    0.9.git20231217,

  * A constructive meeting with sixteen people during the [Guix
    Days](https://guix.gnu.org/en/blog/2024/guix-days-2024-recap/)
    just before [FOSDEM '24](https://archive.fosdem.org/2024/) [with
    notes](https://lists.gnu.org/archive/html/guix-patches/2024-02/msg00035.html)
    that contain some nice ideas,

  * Another [new glibc
    2.39](https://lists.gnu.org/archive/html/guix-commits/2024-02/msg00941.html);
    even better Hurd support, opening the door to `x86_64` support,

  * Yet another [restoring of i586-gnu (32-bit GNU/Hurd)
    support](https://mail.gnu.org/archive/html/guix-patches/2024-08/msg00627.html),

  * The installer just [learnt about the
    Hurd](https://lists.gnu.org/archive/html/guix-patches/2024-10/msg01132.html)!
    More on this below, and finally,

  * Another set of updates: gnumach (1.8+git20240714), mig
    (1.8+git20231217), hurd (0.9.git20240714), netdde (c0ef248d),
    rumpkernel (f1ffd640), and [initial support for x86_64-gnu, aka
    the 64bit
    Hurd](https://lists.gnu.org/archive/html/guix-patches/2024-11/msg00708.html).

# NetDDE and Rumpdisk support

Back in 2020, Ricardo Wurmus [added the NetDDE
package](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=d30d3e3f60df511e484adb3e459625b3251b0917)
that provides Linux 2.6 network drivers.  At the time we didn't get to
integrate and use it though and meanwhile it bitrotted.

After we [resurrected the NetDDE
build](https://lists.gnu.org/archive/html/bug-hurd/2023-05//msg00449.html),
and with [kind help of the Hurd
developers](https://lists.gnu.org/archive/html/bug-hurd/2023-05//msg00455.html)
we finally managed to [support NetDDE for the
Hurd.](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=9c1957921a1f53973c9b4b895f6d6d5fa63fe2dd).
This allows the usage of the Intel 82573L Gigabit Ethernet Controller
of the Thinkpad X60 (and many other network cards, possibly even
WIFI).  Instead of using the builtin kernel driver in GNU Mach, it
would be running *as a userland driver*.

What sparked this development was upstream's NetBSD rumpdisk support
that would allow using modern hard disks such as SSDs, again running
as a userland driver.  Hard disk support builtin in GNU Mach was
once considered to be a nice hack but it only supported disks up to
128 GiB…

First, we needed to [fix the cross build on
Guix](https://lists.gnu.org/archive/html/bug-hurd/2023-05//msg00400.html).

After the [initial attempt at rumpdisk support for the
Hurd](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg00633.html)
it [took
(v2)](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg00659.html)
[some
(v3)](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg00746.html)
[work
(v4)](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg00755.html)
to finally arrive at [rumpdisk support for the Hurd, really, `*`really`*`
(v5)](https://lists.gnu.org/archive/html/guix-patches/2023-05/msg01049.html)

Sadly when actually using them, booting hangs:
```
start: pci.arbiter:
```

What did not really help is that upstream's rumpkernel archive [was
ridiculously
large](https://lists.gnu.org/archive/html/bug-hurd/2023-06/msg00099.html).
We managed to work with upstream to remove unused bits from the
archive.  Upstream created a new archive that instead of 1.8 GiB (!) now
“only” weighs 670 MiB.

Anyway, after a lot of building, rebuilding, and debugging and some
more with [kind help from
upstream](https://lists.gnu.org/archive/html/bug-hurd/2023-05//msg00271.html)
we finally got Rumpdisk and NetDDE [to run in a
Childhurd](https://toot.aquilenet.fr/@civodul/110848429561223704).

![NetDDE and Rumpdisk userland processes in a Childhurd](/static/blog/img/netdde-and-rumpdisk-processes.png)

# Initial Guix/Hurd on the Thinkpad X60

Now that the last (!) `core-updates` merge has finally happened (thanks
everyone!), the recipe of installing Guix/Hurd has been much
simpfilied.  It goes something along these lines.

0) Install Guix/Linux on your X60,

0) Reserve a partition and format it for the Hurd:

    ```
    mke2fs -o hurd -L hurd /dev/sdaX
    ```

0) In your `config.scm`, add some code to add GRUB menuentries for
booting the Hurd, and mount the Hurd partition under `/hurd`:

    ```scheme
    (use-modules (srfi srfi-26)
                 (ice-9 match)
                 (ice-9 rdelim)
                 (ice-9 regex)
                 (gnu build file-systems))

    (define %hurd-menuentry-regex
      "menuentry \"(GNU with the Hurd[^{\"]*)\".*multiboot ([^ \n]*) +([^\n]*)")
    (define (text->hurd-menuentry text)
      (let* ((m (string-match %hurd-menuentry-regex text))
             (label (match:substring m 1))
             (kernel (match:substring m 2))
             (arguments (match:substring m 3))
             (arguments (string-split arguments #\space))
             (root (find (cute string-prefix? "root=" <>) arguments))
             (device-spec (match (string-split root #\=)
                            (("root" device) device)))
             (device (hurd-device-name->device-name device-spec))
             (modules (list-matches "module ([^\n]*)" text))
             (modules (map (cute match:substring <> 1) modules))
             (modules (map (cute string-split <> #\space) modules)))
        (menu-entry
         (label label)
         (device device)
         (multiboot-kernel kernel)
         (multiboot-arguments arguments)
         (multiboot-modules modules))))

    (define %hurd-menuentries-regex
      "menuentry \"(GNU with the Hurd[^{\"]*)\" \\{([^}]|[^\n]\\})*\n\\}")
    (define (grub.cfg->hurd-menuentries grub.cfg)
      (let* ((entries (list-matches %hurd-menuentries-regex grub.cfg))
             (entries (map (cute match:substring <> 0) entries)))
        (map text->hurd-menuentry entries)))

    (define (hurd-menuentries)
      (let ((grub.cfg (with-input-from-file "/hurd/boot/grub/grub.cfg"
                        read-string)))
        (grub.cfg->hurd-menuentries grub.cfg)))

    ...
    (operating-system
       ...
      (bootloader (bootloader-configuration
                   (bootloader grub-bootloader)
                   (targets '("/dev/sda"))
                   (menu-entries (hurd-menuentries))))
      (file-systems (cons* (file-system
                             (device (file-system-label "guix"))
                             (mount-point "/")
                             (type "ext4"))
                           (file-system
                             (device (file-system-label "hurd"))
                             (mount-point "/hurd")
                             (type "ext2"))
                           %base-file-systems))
      ...)
    ```

0) Create a `config.scm` for your Hurd system.  You can get
inspiration from [bare-hurd.tmpl](
https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/system/examples/bare-hurd.tmpl)
and inherit from `%hurd-default-operating-system`.  Use
`grub-minimal-bootloader` and add a `static-networking-service-type`.
Something like:

    ```scheme
    (use-modules (srfi srfi-1) (ice-9 match))
    (use-modules (gnu) (gnu system hurd))

    (operating-system
      (inherit %hurd-default-operating-system)
      (bootloader (bootloader-configuration
                   (bootloader grub-minimal-bootloader)
                   (targets '("/dev/sda"))))
      (kernel-arguments '("noide"))
    ...
      (services
        (cons*
          (service static-networking-service-type
                   (list %loopback-static-networking
                         (static-networking
                          (addresses
                           (list
                            (network-address
                             (device "eth0")
                             (value "192.168.178.37/24"))))
                          (routes
                           (list (network-route
                                  (destination "default")
                                  (gateway "192.168.178.1"))))
                          (requirement '())
                          (provision '(networking))
                          (name-servers '("192.168.178.1")))))
        ...)))
    ```

0) Install the Hurd.  Assuming you have an `ext2` filesystem mounted
on `/hurd`, do something like:

    ```
    guix system build --target=i586-pc-gnu vuurvlieg.hurd --verbosity=1
    sudo -E guix system init --target=i586-pc-gnu --skip-checks \
        vuurvlieg.hurd /hurd
    sudo -E guix system reconfigure vuurvlieg.scm
    ```

0) Reboot and...

Hurray!

We now have [Guix/Hurd running on
Thinkpad](https://todon.nl/@janneke/110451493405777898).

![Guix/Hurd GRUB menu on Thinkpad
X60](/static/blog/img/thinkpad-hurd-bootmenu.jpeg)

![Guix/Hurd running on Thinkpad
X60](/static/blog/img/thinkpad-hurd-login.jpeg)

# Guix/Hurd on Real Iron

While the initial manual install on the X60 was an inspiring
milestone, we can do better.  As mentioned above, just recently the
[installer learnt about the
Hurd](https://lists.gnu.org/archive/html/guix-patches/2024-10/msg01132.html),
right after some smaller problems were addressed, like `guix system
init` creating essential devices for the Hurd, not attempting to run a
cross-built `grub-install` to install Grub, soft-coding the hard-coded
`part:1:device:wd0` root file-system, adding support for booting
Guix/Hurd more than once.

To install Guix/Hurd, first, build a 32bit installation image and copy
it to a USB stick:

```
guix system image --image-type=iso9660 --system=i686-linux gnu/system/install.scm
dd if=/gnu/store/cabba9e-image.iso of=/dev/sdX status=progress
sync
```

then boot it on a not-too-new machine that has wired internet
(although installation over WIFI is possible, there is currently no
WIFI support for the installed Hurd to use it).  On the new `Kernel`
page:

![Installer Kernel page](/static/blog/img/installer-kernel-page.png)

choose `Hurd`.  Do not choose a desktop environment, that's not
available yet.  On the `Network management` page:

![Installer Network management page](/static/blog/img/installer-network-management-page.png)

choose the new `Static networking service`.  In the final
`Configuration file` step, don't forget to edit:

![Installer Configuration file page](/static/blog/img/installer-configuration-file-page.png)

and fill-in your `IP` and `GATEWAY`:

![Installer Edit static networking](/static/blog/img/installer-nano-edit-static-networking.png)

You may want to add some additional packages such as `git-minimal`
from `(gnu packages version-control)` and `sqlite` from `(gnu packages
sqlite)`.

If you also intend to do Guix development on the Hurd—e.g., debugging
and fixing native package builds—then you might want to include all
dependencies to build the guix package, see the
[`devel-hurd.tmpl`](https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/system/examples/devel-hurd.tmpl?h=hurd-team)
for inspiration on how to do that.  Note that any package you add must
already have support for cross-building.

Good luck, and let us know if it works for you and on what kind of
machine, or why it didn't!

# What's next?

In [an earlier
post](https://guix.gnu.org/en/blog/2020/a-hello-world-virtual-machine-running-the-hurd/)
we tried to answer the question “Why bother with the Hurd anyway?”  An
obvious question because it is [all too easy to get
discouraged](https://xkcd.com/1508), to downplay or underestimate the
potential social impact of GNU and the Hurd.

The most pressing problem currently is that the [guix-daemon fails
when invoking `guix authenticate` on the
Hurd](https://issues.guix.gnu.org/73181), which means that we cannot
easily keep our substitutes up to date.  `guix pull` is known to work
but only by pulling from a local branch doing something like:

```
mkdir -p ~/src/guix
cd src/guix
git clone https://git.savannah.gnu.org/git/guix.git master
guix pull --url=~/src/guix/master
```

kinda like we did it in the old days.  Sometimes it seems that guix
does not grok the sqlite store database.  This is needs to be resolved
but as `sqlite` does read it this can be easily worked around by doing
something like:

```
mv /var/guix/db/db.sqlite /var/guix/db/db.sqlite.orig
sqlite3 /var/guix/db/db.sqlite.orig .dump > /var/guix/db/db.sqlite.dump
sqlite3 -init /var/guix/db/db.sqlite.dump /var/guix/db/db.sqlite .quit
```

Also, the boot process still fails to handle an unclean root file
system.  Whenever the Hurd has suffered an unclean shutdown, cleaning
it must currently be done manually, e.g., by booting from the
installer USB stick.

Upstream now has decent support for 64bit (x86_64) albeit [with more
bugs and fewer
packages](https://lists.gnu.org/archive/html/bug-hurd/2024-07/msg00058.html).
As mentioned in the overview [we are now
working](https://lists.gnu.org/archive/html/guix-patches/2024-11/msg00708.html)
[to have
that](https://lists.gnu.org/archive/html/guix-patches/2024-11/msg00925.html)
[in
Guix](https://lists.gnu.org/archive/html/guix-patches/2024-11/msg01326.html)
and [have
made](https://lists.gnu.org/archive/html/guix-patches/2024-11/msg01676.html)
[some progress](https://todon.nl/@janneke/113457925305954698):

![Hello Guix 64bit Hurd](/static/blog/img/hello-guix-x86_64-gnu.png)

more on that in another post.  Other interesting task for Guix
include:

   * Have `guix system reconfigure` work on the Hurd,
   * Figure out WiFi support with NetDDE (and add it to installer!),
   * An [isolated build
        environment](https://issues.guix.gnu.org/43857) (or better
        wait for, err, contribute to the [Guile
        guix-daemon](https://guix.gnu.org/en/blog/2023/a-build-daemon-in-guile/)?),
   * An installer running the Hurd, and,
   * Packages, packages, packages!

We tried to make Hurd development as easy and as pleasant as we could.
As you have seen, things start to work pretty nicely and there is
still plenty of work to do in Guix.  In a way this is “merely
packaging” the amazing work of others.  Some of the real work that
needs to be done and which is being discussed and is in progress right
now includes:

   * [Audio support](https://nlnet.nl/project/Hurd-Audio/) (this is
     sponsored by [NLnet](https://nlnet.nl), thanks!),
   * [Rumpnet](https://lists.gnu.org/archive/html//bug-hurd/2024-05/msg00002.html),
   * [SMP](https://lists.gnu.org/archive/html/bug-hurd/2024-09/msg00053.html),
   * [AArch64](https://lists.gnu.org/archive/html/bug-hurd/2024-03/msg00114.html).

All these tasks look daunting, and indeed that’s a lot of work ahead.
But the development environment is certainly an advantage.  Take an
example: surely anyone who’s hacked on device drivers or file systems
before would have loved to be able to GDB into the code, restart it, add
breakpoints and so on—that’s exactly the experience that the Hurd
offers.  As for Guix, it will make it easy to test changes to the
micro-kernel and to the Hurd servers, and that too has the potential to
speed up development and make it a very nice experience.

Join `#guix` and `#hurd` on
[`libera.chat`](https://guix.gnu.org/en/contact/irc/) or the [mailing
lists](https://guix.gnu.org/en/contact) and get involved!

##### Addendum

It [has been brought to our
attention](https://lists.gnu.org/archive/html/guix-devel/2024-11/msg00276.html)
that people haven't heard that [Debian
GNU/Hurd](https://wiki.debian.org/Debian_GNU/Hurd) has been a reality
for some years now.  While Guix GNU/Hurd has an exciting future,
please be aware that it lacks many packages and services, including
Xorg.  If you would simply like to install the Hurd on bare metal
running your favorite window manager (eg: i3, icewm, etc.)  or
lightweight desktop environment (Xfce) right now, then [installing
Debian
GNU/Hurd](https://cdimage.debian.org/cdimage/ports/latest/hurd-i386/20220824/)
is a good choice.  Though we hope to catch up to them soon!
