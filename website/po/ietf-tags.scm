;;; This file contains an association list for each translation from
;;; the locale to an IETF language tag to be used in the URL path of
;;; translated pages.  The language tag results from the translation
;;; team’s language code from
;;; <https://translationproject.org/team/index.html>.  The underscore
;;; in the team’s code is replaced by a hyphen.  For example, az would
;;; be used for the Azerbaijani language (not az-Latn) and zh-CN would
;;; be used for mainland Chinese (not zh-Hans-CN).
(("en_US" . "en")
 ("cs_CZ" . "cs")
 ("de_DE" . "de")
 ("eo" . "eo")
 ("es_ES" . "es")
 ("fa_IR" . "fa")
 ("fr_FR" . "fr")
 ("ja_JP" . "ja")
 ("ko_KR" . "ko")
 ("lt_LT" . "lt")
 ("nb_NO" . "nb-NO")
 ("pt_BR" . "pt-BR")
 ("ru_RU" . "ru")
 ("sk_SK" . "sk")
 ("sv_SE" . "sv")
 ("tr_TR" . "tr")
 ("uk_UA" . "uk")
 ("zh_CN" . "zh-CN")
 ("zh_TW" . "zh-TW"))
