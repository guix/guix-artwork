;;; GNU Guix web site
;;; Initially written by sirgazil who waives all
;;; copyright interest on this file.

(define-module (apps base templates home)
  #:use-module (apps base templates components)
  #:use-module (apps base templates theme)
  #:use-module (apps base types)
  #:use-module (apps base utils)
  #:use-module (apps blog templates components)
  #:use-module (apps media templates components)
  #:use-module (apps media types)
  #:use-module (apps i18n)
  #:export (home-t))


(define (home-t context)
  "Return the Home page in SHTML using the data in CONTEXT."
  (theme
   #:title (C_ "webpage title"
               '("GNU Guix transactional package manager and distribution"))
   #:description
   (G_ "Guix is a distribution of the GNU operating system.
   Guix is technology that respects the freedom of computer users.
   You are free to run the system for any purpose, study how it
   works, improve it, and share it with the whole world.")
   #:keywords
   (string-split ;TRANSLATORS: |-separated list of webpage keywords
    (G_ "GNU|Linux|Unix|Free software|Libre software|Operating \
system|GNU Hurd|GNU Guix package manager|GNU Guile|Guile \
Scheme|Transactional upgrades|Functional package \
management|Reproducibility") #\|)
   #:active-menu-item (C_ "website menu" "Overview")
   #:css (list
	  (theme-url "css/item-preview.css")
	  (theme-url "css/index.css")
          (theme-url "css/video-preview.css"))
   #:content
   `(main
     ;; Featured content.
     (section
      (@ (class "featured-content"))
      ,(G_ `(h2 (@ (class "a11y-offset")) "Summary"))

      (div
       (@ (class "summary centered-block centered-text limit-width"))
       ;; TRANSLATORS: Package Management, Invoking guix time-machine and
       ;; Invoking guix deploy are section names in the English (en)
       ;; manual.
       ,(G_
         `(p
           "GNU Guix is a "
           ,(G_ (manual-link-yellow
                 "package manager"
                 (G_ "en")
                 (G_ "Package-Management.html")))
           " for GNU/Linux systems. It is designed to give users "
           ,(G_ `(b "more control"))  ; TODO: Link to relevant documentation.
           " over their general-purpose and specialized computing \
environments, and make these easier to "
           ,(G_ (manual-link-yellow
                 "reproduce over time"
                 (G_ "en")
                 (G_ "Invoking-guix-time_002dmachine.html")))
           " and "
           ,(G_ (manual-link-yellow
                 "deploy"
                 (G_ "en")
                 (G_ "Invoking-guix-deploy.html")))
           " to one or many devices.")))

      (div
       (@ (class "action-box centered-text"))
       ,(button-big
         #:label (C_ "button" "FEATURES")
         ;; TRANSLATORS: Features is a section name in the English (en)
         ;; manual.
         #:url (G_ (manual-url-with-language (G_ "en") "Features.html"))
         #:light #true)
       " " ; A space for readability in non-CSS browsers.
       ,(button-big
         #:label (C_ "button" "DOWNLOAD")
         #:url (guix-url "download/")
         #:light #true)
       " " ; A space for readability in non-CSS browsers.
       ,(button-big
         #:label (C_ "button" "CONTRIBUTE")
	 #:url (guix-url "contribute/")
	 #:light #true)))

     ;; Give Users control.
     (section
      (@ (class "panel panel-dark"))

      (div
       (@ (class "limit-width centered-block"))

       (div
        ,(G_ '(h2 "Give Users Control"))
        ,(G_ '(p "Users on the same machine can manage their own \
packages independently from each other, without superuser privileges."))
        ,(button-big
          #:label (C_ "button" "LEARN MORE")
          ;; TRANSLATORS: Package Management is a section name in the
          ;; English (en) manual.
          #:url (G_ (manual-url-with-language
                     (G_ "en")
                     "Package-Management.html"))
          #:light #true))

       (img
        (@ (src ,(guix-url "static/base/img/give-users-control.svg"))
           (alt "")))))

     ;; Reproducible Outputs.
     (section
      (@ (id "reproducible-outputs")
         (class "discovery-box"))

      (div
       (@ (class "limit-width centered-block"))

       ,(G_ '(h2 "Reproducible Outputs"))
       ,(G_
         `(p "Guix provides "
             ,(G_ (link-yellow #:label "thousands of packages"
                               #:url (packages-url)))
             " which include desktop environments, applications, \
system tools, programming languages and their data and other \
digital artifacts which work as the building blocks for creating "
             ,(G_ (link-yellow #:label "reproducible"
                               #:url "\
https://en.wikipedia.org/wiki/Reproducible_builds"))
             " shell environments, containers and systems for \
different computing needs.")))

      (div
       (@ (class "grid centered-content text-larger"))

       (div (@ (class "stack"))
            ;; TRANSLATORS: Defining Packages is a section name in the
            ;; English (en) manual.
            ,(G_ (manual-link-yellow
                  "Package"
                  (G_ "en")
                  (G_ "Defining-Packages.html")))
            (img
             (@ (src ,(guix-url "static/base/img/reproducible-outputs-package.svg"))
                (alt ""))))
       (div (@ (class "stack"))
            ;; TRANSLATORS: Invoking guix shell is a section name in the
            ;; English (en) manual.
            ,(G_ (manual-link-yellow
                  "Shell Environment"
                  (G_ "en")
                  (G_ "Invoking-guix-shell.html")))
            (img
             (@ (src ,(guix-url "static/base/img/reproducible-outputs-shell.svg"))
                (alt ""))))
       (div (@ (class "stack"))
            ;; TRANSLATORS: Invoking guix container is a section name in the
            ;; English (en) manual.
            ,(G_ (manual-link-yellow
                  "Container"
                  (G_ "en")
                  (G_ "Invoking-guix-container.html")))
            (img
             (@ (src ,(guix-url "static/base/img/reproducible-outputs-container.svg"))
                (alt ""))))
       (div (@ (class "stack"))
            ;; TRANSLATORS: System Configuration is a section name in the
            ;; English (en) manual.
            ,(G_ (manual-link-yellow
                  "Systems"
                  (G_ "en")
                  (G_ "System-Configuration.html")))
            (img
             (@ (src ,(guix-url "static/base/img/reproducible-outputs-systems.svg"))
                (alt "")))))

      (div
       (@ (id "timeline")
          (class "limit-width text-larger"))

       ,(G_ '(p (@ (id "timeline-today")) "Build an environment today"))
       (p (@ (id "timeline-lapse"))
          ;; TRANSLATORS: Invoking guix time-machine is a section name in the
          ;; English (en) manual.
          ,(G_ (manual-link-yellow
                "Reproduce in time"
                (G_ "en")
                (G_ "Invoking-guix-time_002dmachine.html"))))
       ,(G_ `(p (@ (id "timeline-future"))
                "Get the same environment in the future"
                ;; TRANSLATORS: The * indicates there's more information in another place.
                ,(G_ (link-yellow
                      #:label "*"
                      #:url (guix-url "blog/2024/adventures-on-the-quest-for-long-term-reproducible-deployment/"))))))

      ,(button-big
        #:label (C_ "button" "EXPLORE PACKAGES")
        #:url (packages-url)
        #:light #true))

     ;; One Language for Everything.
     (section
      (@ (id "one-language")
         (class "panel panel-dark"))

      (div
       (@ (class " limit-width centered-block"))

       (div
        ,(G_ '(h2 "One Language for Everything"))
        ,(G_ '(p "Use Guile Scheme APIs, including high-level embedded \
domain-specific languages (EDSLs) to define packages and whole-system \
configurations."))
        ,(button-big
          #:label (C_ "button" "CHECK THE API")
          ;; TRANSLATORS: Programming Interface is a section name in the
          ;; English (en) manual.
          #:url (G_ (manual-url-with-language
                     (G_ "en")
                     "Programming-Interface.html"))
          #:light #true)
        " " ; A space for readability in non-CSS browsers.
        ,(button-big
          #:label (C_ "button" "WHAT'S GUILE")
          #:url (gnu-url "software/guile/")
          #:light #true))

       (img
        (@ (src ,(guix-url "static/base/img/guile-logo-outlined-floating.svg"))
           (alt "")))))

     ;; All of It, Free Software.
     (section
      (@ (id "all-free-software")
         (class "panel panel-light"))

      (div
       (@ (class " limit-width centered-block"))

       (div
        ,(G_ '(h2 "All of It, Free Software"))
        ,(G_ '(p "Guix is a GNU Project—which respects the freedom of \
computer users. You are free to use, study, modify, and share Guix and \
all the packages it provides."))
        ,(button-big
          #:label (C_ "button" "LEARN MORE")
          #:url (gnu-url (G_ "philosophy/free-sw.en.html"))))

       (img
        (@ (src ,(guix-url "static/base/img/people-focused.svg"))
           (alt "")))))

     ;; GNU Guix in Your Field.
     (section
      (@ (id "guix-in-your-field")
         (class "panel-dark"))

      ,(G_ '(h2 "GNU Guix in Your Field"))

      (div
       (@ (class "limit-width centered-block"))

       " " ; A space for readability in non-CSS browsers (same below).
       ,(button-big
         #:label (C_ "button" "SOFTWARE DEVELOPMENT")
         #:url (guix-url "blog/tags/software-development/")
         #:light #true)
       " "
       ,(button-big
         #:label (C_ "button" "BIOINFORMATICS")
         #:url (guix-url "blog/tags/bioinformatics/")
         #:light #true)
       " "
       ,(button-big
         #:label (C_ "button" "HIGH PERFORMANCE COMPUTING")
         #:url (guix-url "blog/tags/high-performance-computing/")
         #:light #true)
       " "
       ,(button-big
         #:label (C_ "button" "RESEARCH")
         #:url (guix-url "blog/tags/research/")
         #:light #true)
       " "
       ,(button-big
         #:label (C_ "button" "ALL FIELDS...")
         #:url (guix-url "blog/")
         #:light #true)))

     ;; Get Guix.
     (section
      (@ (class "panel panel-dark"))

      (div
       (@ (class "limit-width centered-block"))

       (div
        ,(G_ '(h2 "Get Guix"))
        ,(G_ '(p "You can use Guix on top of any GNU/Linux \
distribution of your preference. It won't clash with your distro's \
package manager."))
        ,(button-big
          #:label (C_ "button" "DOWNLOAD")
          #:url (guix-url "download/")
          #:light #true))

       (img
        (@ (src ,(guix-url "static/base/img/guix-shell.svg"))
           (alt "")))))

     ;; Get the Guix System.
     (section
      (@ (id "get-guix-system")
         (class "panel panel-dark"))

      (div
       (@ (class "limit-width centered-block"))

       (div
        ,(G_ '(h2 "Get the Guix System"))
        ,(G_ '(p "A complete GNU operating system harnessing all the \
capabilities of the Guix software. Spawned by Guix itself."))
        ,(button-big
          #:label (C_ "button" "OVERVIEW")
          ;; TRANSLATORS: System Installation is a section name in the
          ;; English (en) manual.
          #:url (G_ (manual-url-with-language
                     (G_ "en")
                     "System-Installation.html"))
          #:light #true)
        " " ; A space for readability in non-CSS browsers.
        ,(button-big
          #:label (C_ "button" "DOWNLOAD")
          #:url (guix-url "download/")
          #:light #true))

       (img
        (@ (src ,(guix-url "static/base/img/guix-system-display.svg"))
           (alt "")))))

     ;; Instructional Videos.
     (section
      (@ (id "instructional-videos")
         (class "discovery-box"))

      (div
       (@ (class "limit-width centered-block"))

       ,(G_ '(h2 "Instructional Videos")))

      (div
       ,@(map
          (lambda (item)
            (cond ((video? item) (video-preview item))
                  (else (playlist-preview item))))
          (context-datum context "videos")))

      (div
       (@ (class "action-box"))

       ,(button-big
         #:label (C_ "button" "ALL VIDEOS")
         #:url (guix-url "videos/")
         #:light #true)))

     ;; Latest Blog posts.
     (section
      (@ (id "blog-latest-posts")
         (class "panel-dark centered-text"))
      ,(G_ `(h2 "Blog"))

      ,@(map post-preview (context-datum context "posts"))

      (div
       (@ (class "action-box centered-text"))
       ,(button-big
         #:label (C_ "button" "ALL POSTS")
         #:url (guix-url "blog/")
         #:light #true)))

     ;; Contact info.
     (section
      (@ (id "contact-info")
         (class "panel-dark centered-text"))
      ,(G_ `(h2 "Contact"))

      ,@(map contact-preview (context-datum context "contact-media"))

      (div
       (@ (class "action-box centered-text"))
       ,(button-big
         #:label (C_ "button" "ALL CONTACT MEDIA")
         #:url (guix-url "contact/")
         #:light #true))))))
