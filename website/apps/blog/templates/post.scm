;;; GNU Guix web site
;;; Copyright © 2023 Florian Pelz <pelzflorian@pelzflorian.de>
;;; Initially written by sirgazil who waives all
;;; copyright interest on this file.

(define-module (apps blog templates post)
  #:use-module (apps base templates components)
  #:use-module (apps base templates theme)
  #:use-module (apps base types)
  #:use-module (apps base utils)
  #:use-module (apps blog utils)
  #:use-module ((apps blog templates components) #:prefix blog:)
  #:use-module (apps i18n)
  #:use-module (haunt post)
  #:use-module (srfi srfi-19)
  #:export (post-t))


(define (post-t context)
  "Return a page in SHTML for the post in the given CONTEXT."
  (let* ((post (context-datum context "post"))
	 (tags (post-ref post 'tags)))
    (theme
     #:title (list (post-ref post 'title)
		   (date->string (post-date post) "~Y")
                   (C_ "webpage title" "Blog"))
     #:description
     (G_ "Blog posts about GNU Guix.")
     #:keywords tags
     #:active-menu-item (C_ "website menu" "Blog")
     #:css
     (list (theme-url "css/page.css")
           (theme-url "css/code.css")
           (theme-url "css/post.css"))
     #:crumbs
     (list (crumb (C_ "website menu" "Blog") (guix-url "blog/"))
	   (crumb (post-ref post 'title)
		  (guix-url (post-url-path post))))
     #:content
     `(main
       (article
        (@ (class "page centered-block limit-width") (lang "en"))
	(h2 ,(post-ref post 'title))
	(p
         (@ (class "post-metadata centered-text") (lang ,%current-ietf-tag))
	 ,(post-ref post 'author) " — "
         ,(date->string (post-date post) (C_ "SRFI-19 date->string format"
                                             "~B ~e, ~Y")))

	,(change-image-to-video
          (syntax-highlight (post-sxml post)))

	(div
         (@ (class "tag-list") (lang ,%current-ietf-tag))
         ,(G_ `(p "Related topics:"))

	 ,@(map
	    (lambda (tag)
	      (list
	       (button-little
		#:label tag
		#:url (guix-url (tag-url-path tag)))
	       " ")) ; NOTE: Force space for readability in non-CSS browsers.
	    (sort tags tag-first?)))

        (div
         (@ (class "license") (lang ,%current-ietf-tag))
         ,(G_ `(p "Unless otherwise stated, blog posts on this site are
copyrighted by their respective authors and published under the terms of
the " ,(G_
                    `(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
                        "CC-BY-SA 4.0"))
                  " license and those of the "
                  ,(G_
                    `(a (@ (href
                            "https://www.gnu.org/licenses/fdl-1.3.html"))
                        "GNU Free Documentation License"))
                  " (version 1.3 or later, with no Invariant Sections, no
Front-Cover Texts, and no Back-Cover Texts)."))))))))
